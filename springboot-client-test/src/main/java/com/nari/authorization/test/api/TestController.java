package com.nari.authorization.test.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @CreateTime: 2024-03-21 16:33
 * @Description:
 * @Author: WH
 */
@RestController
@RequestMapping("tests")
@Slf4j
public class TestController {


    /**
     * 没有权限标记的接口，不允许直接访问，也会跳转到登录页面，登录成功后，只要有登录凭证就可以正常访问
     */
    @GetMapping("test1")
    public String test1(){
        return "没有@PreAuthorize权限注解的接口";
    }

    /**
     * 有权限标记的接口， 只允许在授权页面的SCOPE中有EXCELLENT的才能访问，默认会自动添加SCOPE_
     */
    @GetMapping("test2")
    @PreAuthorize("hasAuthority('SCOPE_EXCELLENT')")
    public String test2(){
        return"权限标记是SCOPE_EXCELLENT的";
    }

    /**
     * 有权限标记的接口， 默认会自动添加SCOPE_,所以无法访问这个接口
     */
    @GetMapping("test3")
    @PreAuthorize("hasAnyAuthority('EXCELLENT','XXXX')")
    public String test3(){
        return "权限标记是EXCELLENT的";
    }

}
