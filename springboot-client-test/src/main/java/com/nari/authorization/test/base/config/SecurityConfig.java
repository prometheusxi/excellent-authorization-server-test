package com.nari.authorization.test.base.config;

import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.web.SecurityFilterChain;

/**
 * @CreateTime: 2024-04-02 17:15
 * @Description:
 * @Author: WH
 */
@Configuration
@EnableMethodSecurity
@EnableWebSecurity
@Slf4j
public class SecurityConfig {

    @Resource
    private ClientRegistrationRepository clientRegistrationRepository;

    @Bean
    public SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
        //关闭这两个没用的
        http.csrf(AbstractHttpConfigurer::disable);
        http.cors(AbstractHttpConfigurer::disable);
        //拦截所有请求
        http.authorizeHttpRequests((authorizationManagerRequestMatcherRegistry ->
            authorizationManagerRequestMatcherRegistry
                .anyRequest()
                .authenticated()
        ));
        //必须配置！！！！！
        http.oauth2Login(login -> login.clientRegistrationRepository(clientRegistrationRepository));
        http.oauth2Client(client -> client
            .clientRegistrationRepository(clientRegistrationRepository)
        );
        return http.build();
    }

}
