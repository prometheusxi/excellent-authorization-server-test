package com.nari.authorization.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootClientTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootClientTestApplication.class, args);
	}

}
