package com.nari.authorization.test.base;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

/**
 * @CreateTime: 2024-03-13 11:35
 * @Description:
 * @Author: WH
 */
public class JsonUtils {

    public static final ObjectMapper MAPPER = new ObjectMapper();


    public static String objToJson(Object data) {
        try {
            if (data == null) {
                return null;
            }
            return MAPPER.writeValueAsString(data);
        } catch (JsonProcessingException e) {
            System.err.println(e.getMessage());
        }
        return null;
    }

    public static <T> T jsonToObj(String jsonData, Class<T> beanType) {
        try {
            return MAPPER.readValue(jsonData, beanType);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return null;
    }

    public static <T> List<T> jsonToList(String jsonData, Class<T> beanType) {
        JavaType javaType = MAPPER.getTypeFactory().constructParametricType(List.class, beanType);
        try {
            return MAPPER.readValue(jsonData, javaType);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return null;
    }
}
