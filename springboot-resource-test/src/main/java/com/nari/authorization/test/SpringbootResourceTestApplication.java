package com.nari.authorization.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootResourceTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootResourceTestApplication.class, args);
	}

}
