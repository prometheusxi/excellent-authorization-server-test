package com.nari.authorization.test.api;

import com.nari.authorization.test.base.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @CreateTime: 2024-06-23 11:51
 * @Description:
 * @Author: WH
 */
@RestController
@RequestMapping("authorization")
@Slf4j
public class AuthorizationServerServiceSupportController {

    @PostMapping("/server/loadUserByUsername")
    @PreAuthorize("hasAnyAuthority('SCOPE_AUTHORIZATION_SERVER','AUTHORIZATION_SERVER')")
    public UserDetailsImpl loadUserByUsername(@RequestBody Map<String, Object> additionalParameters) {
        Object usernameObj = additionalParameters.get("username");
        return getUserDetailsImpl();
    }

    @PostMapping("/server/inspectDevice")
    @PreAuthorize("hasAnyAuthority('SCOPE_AUTHORIZATION_SERVER','AUTHORIZATION_SERVER')")
    public String inspectDevice(@RequestBody Map<String, Object> additionalParameters) {
        String s = JsonUtils.objToJson(additionalParameters);
        System.out.println(s);
        String serialNumber = (String) additionalParameters.get("serialNumber");
        return "haha123456".equals(serialNumber) ?"1":"0";
    }

    @PostMapping("/server/loadUserByDirect")
    @PreAuthorize("hasAnyAuthority('SCOPE_AUTHORIZATION_SERVER','AUTHORIZATION_SERVER')")
    public UserDetailsImpl loadUserByDirect(@RequestBody Map<String, Object> additionalParameters) {
        String s = JsonUtils.objToJson(additionalParameters);
        System.out.println(s);
        return getUserDetailsImpl();

    }

    private UserDetailsImpl getUserDetailsImpl(){
        UserDetailsImpl userDetails = new UserDetailsImpl();
        userDetails.setUserId("1");
        userDetails.setUsername("admin");
        userDetails.setPassword(new BCryptPasswordEncoder(10).encode("123456"));
        userDetails.setAuthorities(Stream.of("EDIT","ADD","DELETE").map(SimpleGrantedAuthority::new).collect(Collectors.toList()));
        userDetails.setEnabled(true);
        userDetails.setAccountNonExpired(true);
        userDetails.setAccountNonLocked(true);
        userDetails.setCredentialsNonExpired(true);
        return userDetails;
    }

    public  class UserDetailsImpl implements UserDetails {

        private String clientId;

        private String userId;

        private String username;

        private String password;

        //不传就用认证授权服务的默认密码解析器，传就根据盐值重写创建！！！哈哈哈，为了适配各种不一样的数据支持系统！！！
        private Integer passwordStrength;

        private String telephone;

        private String email;

        private Boolean enabled;

        private Boolean accountNonExpired;

        private Boolean credentialsNonExpired;

        private Boolean accountNonLocked;

        private List<SimpleGrantedAuthority> authorities;

        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
            return this.authorities;
        }

        @Override
        public String getPassword() {
            return this.password;
        }

        @Override
        public String getUsername() {
            return this.username;
        }

        @Override
        public boolean isAccountNonExpired() {
            return this.accountNonExpired;
        }

        @Override
        public boolean isAccountNonLocked() {
            return this.accountNonLocked;
        }

        @Override
        public boolean isCredentialsNonExpired() {
            return this.credentialsNonExpired;
        }

        @Override
        public boolean isEnabled() {
            return this.enabled;
        }


        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public Integer getPasswordStrength() {
            return passwordStrength;
        }

        public void setPasswordStrength(Integer passwordStrength) {
            this.passwordStrength = passwordStrength;
        }

        public String getTelephone() {
            return telephone;
        }

        public void setTelephone(String telephone) {
            this.telephone = telephone;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Boolean getEnabled() {
            return enabled;
        }

        public void setEnabled(Boolean enabled) {
            this.enabled = enabled;
        }

        public Boolean getAccountNonExpired() {
            return accountNonExpired;
        }

        public void setAccountNonExpired(Boolean accountNonExpired) {
            this.accountNonExpired = accountNonExpired;
        }

        public Boolean getCredentialsNonExpired() {
            return credentialsNonExpired;
        }

        public void setCredentialsNonExpired(Boolean credentialsNonExpired) {
            this.credentialsNonExpired = credentialsNonExpired;
        }

        public Boolean getAccountNonLocked() {
            return accountNonLocked;
        }

        public void setAccountNonLocked(Boolean accountNonLocked) {
            this.accountNonLocked = accountNonLocked;
        }

        public void setAuthorities(List<SimpleGrantedAuthority> authorities) {
            this.authorities = authorities;
        }

        public String getClientId() {
            return clientId;
        }

        public void setClientId(String clientId) {
            this.clientId = clientId;
        }
    }

}
