package com.nari.authorization.test.base.config;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;
import org.springframework.security.web.SecurityFilterChain;

/**
 * @CreateTime: 2024-04-02 17:15
 * @Description:
 * @Author: WH
 */
@Configuration
@EnableMethodSecurity
@EnableWebSecurity
@Slf4j
public class SecurityConfig {



    @Bean
    public SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
        // 1.全部都需要权限，已经配置不需要权限的接口
        http.authorizeHttpRequests(authorize -> authorize.anyRequest().authenticated());
        // 1. oauth2.jwt(Customizer.withDefaults()) 此处添加自定义解析设置
        http.oauth2ResourceServer(oauth2 -> oauth2.jwt(Customizer.withDefaults()));
        //2.自定义自定义异常响应配置
        http.exceptionHandling((exceptionHandlingConfigurer)->{
            exceptionHandlingConfigurer.accessDeniedHandler((HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException)->{
                response.setStatus(403);
                response.setHeader("Content-Type", "application/json; charset=UTF-8");
                response.getWriter().write("{\"code\":"+403+",\"msg\":\"权限不足\"}");
                response.flushBuffer();
            });
            exceptionHandlingConfigurer.authenticationEntryPoint((HttpServletRequest request, HttpServletResponse response, AuthenticationException authException)->{
                response.setHeader("Content-Type", "application/json; charset=UTF-8");
                response.setStatus(401);
                response.getWriter().write("{\"code\":"+401+",\"msg\":\"请从新登录\"}");
                response.flushBuffer();
            });
        });
        return http.build();
    }

    /**
     * 特别注意！！！需要一个和认证授权服务一样的jwtAuthenticationConverter，不然解析会有问题
     */
    private static final String AUTHORITIES_NAME = "authorities";
    @Bean
    public JwtAuthenticationConverter jwtAuthenticationConverter(){
        JwtGrantedAuthoritiesConverter jwtGrantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
        jwtGrantedAuthoritiesConverter.setAuthorityPrefix("");
        jwtGrantedAuthoritiesConverter.setAuthoritiesClaimName(AUTHORITIES_NAME);
        JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(jwtGrantedAuthoritiesConverter);
        return jwtAuthenticationConverter;
    }

}
